package Eclipse;

public class Technologies {
	
	private Technology tech[] = new Technology[38];
	
	public Technologies() {
		tech[0] = new Technology("Neutron Bombs", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Destroy other player's cubes at turn end.", 1, 2, 2, 0, false, 0, 0, 0);
		tech[1] = new Technology("Starbase", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Allows you to strat buidling starbases", 1, 4, 3, 0, false, 0, 0, 0);
		tech[2] = new Technology("Plasma Cannon", 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, "", 1, 6, 4, 0, false, 2, 0, 0);
		tech[3] = new Technology("Phase Shield", 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 1, 8, 5, 0, false, 1, 0, 0);
		tech[4] = new Technology("Advanced Mining", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "You can now place cubes on special mining squares", 1, 10, 6, 0, false, 0, 0, 0);
		tech[5] = new Technology("Tachyon Source", 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, "", 1, 12, 6, 0, false, 0, 0, 0);
		tech[6] = new Technology("Plasma Missile", 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, "", 1, 14, 7, 0, false, 0, 0, 0);
		tech[7] = new Technology("Gluon Computer", 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, "", 1, 16, 8, 0, false, 2, 2, 0);
		tech[8] = new Technology("Gauss Shield", 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 2, 2, 2, 0, false, 0, 0, 0);
		tech[9] = new Technology("Improved Hull", 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, "", 2, 4, 3, 0, false, 0, 0, 0);
		tech[10] = new Technology("Fusion Source", 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, "", 2, 6, 4, 0, false, 0, 0, 0);
		tech[11] = new Technology("Positron Computer", 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, "", 2, 8, 5, 0, false, 1, 1, 0);
		tech[12] = new Technology("Advanced Economy", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "You can now place cubes on special economy squares", 2, 10, 6, 0, false, 0, 0, 0);
		tech[13] = new Technology("Tachyon Drive", 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, "", 2, 12, 6, 0, false, 3, 3, 0);
		tech[14] = new Technology("Antimatter Cannon", 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, "", 2, 14, 7, 0, false, 4, 0, 0);
		tech[15] = new Technology("Quantum Grid", 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, "", 2, 16, 8, 0, false, 0, 0, 0);
		tech[16] = new Technology("Nanobots", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Increase the number of spacecraft you can build by 1", 3, 2, 2, 1, false, 0, 0, 0);
		tech[17] = new Technology("Fusion Drive", 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, "", 3, 4, 3, 0, false, 2, 2, 0);
		tech[18] = new Technology("Advanced Robotics", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, "", 3, 6, 4, 0, false, 0, 0, 0);
		tech[19] = new Technology("Orbital", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "You can now build orbitals", 3, 8, 5, 0, false, 0, 0, 0);
		tech[20] = new Technology("Advanced Labs", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "You can now place cubes on special science squares", 3, 10, 6, 0, false, 0, 0, 0);
		tech[21] = new Technology("Monolith", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "You can now build monoliths", 3, 12, 6, 0, false, 0, 0, 0);
		tech[22] = new Technology("Artifact Key", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "When bought gain 5 resources for each star on a tile you control", 3, 14, 6, 0, false, 0, 0, 0);
		tech[23] = new Technology("Wormhole Generator", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "You can now travel through partial warpholes", 3, 16, 8, 0, false, 0, 0, 0);
		tech[24] = new Technology("Ion Cannon", 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, "", 4, 0, 0, 0, true, 1, 0, 0);
		tech[25] = new Technology("Electron Computer", 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, "", 4, 0, 0, 0, true, 0, 0, 0);
		tech[26] = new Technology("Nuclear Drive", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 4, 0, 0, 0, true, 1, 1, 0);
		tech[27] = new Technology("Nuclear Source", 0, 0, 3, 0, 0, 0, 0, 0, 0, 1, "", 4, 0, 0, 0, true, 0, 0, 0);
		tech[28] = new Technology("Hull", 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, "", 4, 0, 0, 0, true, 0, 0, 0);
		tech[29] = new Technology("Hypergrid Source", 0, 0, 11, 0, 0, 0, 0, 0, 0, 0, "", 5, 0, 0, 0, false, 0, 0, 0);
		tech[30] = new Technology("Muon Source", 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, "", 5, 0, 0, 0, false, 0, 1, 0);
		tech[31] = new Technology("Ion Turret", 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, "", 5, 0, 0, 0, false, 1, 0, 0);
		tech[32] = new Technology("Axion Computer", 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, "", 5, 0, 0, 0, false, 0, 0, 0);
		tech[33] = new Technology("Morph Shield", 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 5, 0, 0, 0, false, 0, 2, 1);
		tech[34] = new Technology("Ion Disruptor", 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, "", 5, 0, 0, 0, false, 0, 3, 0);
		tech[35] = new Technology("Shard Hull", 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, "", 5, 0, 0, 0, false, 0, 0, 0);
		tech[36] = new Technology("Jump Drive", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Can now move to adjacent squares regardless of warphole", 5, 0, 0, 0, false, 0, 2, 0);
		tech[37] = new Technology("Conformal Drive", 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, "", 5, 0, 0, 0, false, 2, 2, 0);
	}
	
	public int findTechnology(String name) {
		for (int x=0; x<38; x++) {
			if (tech[x].getName().equalsIgnoreCase(name)) {
				return x;
			}
		}
		Main.Output("This is not a valid technology, please try again.");
		String input = Main.stringInput();
		findTechnology(input);
		return 0;
	}
	
	public String toString() {
		String string = "You have the following technologies that you can put on your spacecraft\n";
		for (int x=0; x<38; x++) {
			if (tech[x].getStatus()) {
				string = "; " + string+tech[x].getName();
			}
		}
		return string;
	}
	
	public String outputTechnology(String name) {
		int x = findTechnology(name);
		String string = "Technology Name: " + tech[x].getName() + "\n";
		if (tech[x].getShield() != 0)
			string = string + "Shield Amount: -" + tech[x].getShield() + "\n";
		if (tech[x].getComputer() != 0)
			string = string + "Computer Amount: +" + tech[x].getComputer() + "\n";
		if (tech[x].getEnergySource() != 0)
			string = string + "Energy Total: " + tech[x].getEnergySource() + "\n";
		if (tech[x].getHullIncrease() != 0)
			string = string + "Hull Increase: " + tech[x].getHullIncrease() + "\n";
		if (tech[x].getYellowDice() != 0)
			string = string + "Yellow Dice Added: " + tech[x].getYellowDice() + "\n";
		if (tech[x].getOrangeDice() != 0)
			string = string + "Orange Dice Added: " + tech[x].getOrangeDice() + "\n";
		if (tech[x].getRedDice() != 0)
			string = string + "Red Dice Added: " + tech[x].getRedDice() + "\n";
		if (tech[x].getMissileDice() != 0)
			string = string + "Missile Dice Added: " + tech[x].getMissileDice() + "\n";
		if (tech[x].getDiscsAdded() != 0)
			string = string + "Discs Added: " + tech[x].getDiscsAdded() + "\n";
		if (tech[x].getMoveAdded() != 0)
			string = string + "Move Added: " + tech[x].getMoveAdded() + "\n";
		if (tech[x].getMaxPrice() != 0)
			string = string + "Max Price: " + tech[x].getMaxPrice() + "\n";
		if (tech[x].getMinPrice() != 0)
			string = string + "Min Price: " + tech[x].getMinPrice() + "\n";
		if (tech[x].getPowerNeeded() != 0)
			string = string + "Power Needed to Use: " + tech[x].getPowerNeeded() + "\n";
		if (tech[x].getSpeedIncrease() != 0)
			string = string + "Speed Increase: " + tech[x].getSpeedIncrease() + "\n";
		if (tech[x].getType() != 0)
			string = string + "Type: " + tech[x].getType() + "\n";
		if (tech[x].getSpecialPower() != "")
			string = string + "Special Power: " + tech[x].getSpecialPower() + "\n";
		string = string + "Owned: ";
		if (tech[x].getStatus())
			string = string + "Yes";
		else 
			string = string + "No";
		return string;
	}
}
