package Eclipse;

public class Cubes {
	int Num;
	public Cubes (int x) {
		Num = x;
	}
	
	public void addCubes(int x) {
		if (Num == 11)
			System.out.println("Cannot add cubes as you already have placed 11 cubes on this track");
		else 
			Num = Num+x;
	}
	
	public void removeCubes(int x) {
		if (Num == 0)
			System.out.println("Cannot remove cubes as you have already placed 11 cubes on Hexes");
		else
			Num = Num-x;
	}
	
	public int getCubes() {
		return Num;
	}
	
	public int getReturn() {
		if (Num == 11)
			return 2;
		else if (Num == 10)
			return 3;
		else if (Num == 9)
			return 4;
		else if (Num == 8)
			return 6;
		else if (Num == 7)
			return 8;
		else if (Num == 6)
			return 10;
		else if (Num == 5)
			return 12;
		else if (Num == 4)
			return 15;
		else if (Num == 3)
			return 18;
		else if (Num == 2)
			return 21;
		else if (Num == 1)
			return 24;
		else if (Num == 0)
			return 28;
		else
			return 0;
	}
	
	public String toString (String Resource) {
		return "You have " + Num + "cubes placed on the" + Resource + "track. This means you will recieve " + getReturn() + "of " + Resource + ".";
	}
}
