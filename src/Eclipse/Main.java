package Eclipse;

import java.util.Scanner;

public class Main {
	public int TotalMoney, TotalMaterials, TotalScience, disksOnTrack, disksOnHex, disksOnBoard, buildNum;
	SpaceCraft Starbase, Dreadnought, Cruiser, Interceptor;
	Cubes MoneyTrack, MaterialsTrack, ScienceTrack;
	
	
	private Technologies mTech;

	public static void main(String[] Args) {
		Main game = new Main();
		
		game.HumanStart();
		game.playGame();
	}
	
	public Main() {
		mTech = new Technologies();
		Output(mTech.outputTechnology("Neutron Bombs"));
	}

	public void playGame() {
		int count = 0;
		do {
			count++;
			Output("This is the start of turn " + count);
			diskTurnStart();
			boolean turnContinue = true;
			do {
				int choice = chooseAction();
				Output(String.valueOf(choice));
				if (choice >= 1 && choice <= 6) {
					disksOnBoard++;
					disksOnTrack--;
				}
				if (choice == 1)
					Explore();
				else if (choice == 2)
					Influence();
				else if (choice == 3)
					Research();
				else if (choice == 4)
					Upgrade();
				else if (choice == 5)
					Build();
				else if (choice == 6)
					Move();
				else if (choice == 7)
					PlaceCubes();
				else if (choice == 8)
					Pass();
				else if (choice == 9)
					CraftOutput();
				else if (choice == 10)
					TechnoOutput();
			} while (turnContinue);
		} while (count <= 9);
	}
	
	public void HumanStart() {
		Interceptor = new SpaceCraft(1, 1, 0, 1, 0, 3, 1, 0, 0, 3, 0);
		Cruiser = new SpaceCraft(1, 1, 0, 1, 0, 3, 2, 0, 1, 5, 0);
		Dreadnought = new SpaceCraft(1, 1, 0, 2, 0, 3, 3, 0, 1, 8, 0);
		Starbase = new SpaceCraft(0, 4, 0, 1, 0, 3, 3, 0, 1, 3, 0);
		TotalMoney = 2;
		TotalMaterials = 3;
		TotalScience = 3;
		MoneyTrack = new Cubes(3);
		MaterialsTrack = new Cubes(3);
		ScienceTrack = new Cubes(3);
		disksOnTrack = 12;
		disksOnHex = 1;
		disksOnBoard = 0;
	}

	public int chooseAction() {
		Output("You have " + disksOnTrack + " disks to use this turn. You are currently paying " + costThisTurn());
		Output("What would you like to do?");
		Output("1. Explore");
		Output("2. Influence");
		Output("3. Research");
		Output("4. Updgrade");
		Output("5. Build");
		Output("6. Move");
		Output("7. Place Cubes");
		Output("8. Pass");
		Output("9. Spacecraft Information");
		Output("10. Technonolgy Available");
		int choice = intInput();
		return choice;
	}

	public void Explore() {
		Output("Did you place a disk on the tile? (Enter 1 for yes or 2 for no)");
		int choice = intInput();
		if (choice == 1) {
			disksOnHex++;
			disksOnTrack--;
		}
		Output("----------------------------------");
	}

	public void Influence() {
		Output("How many disks did you place on the hex?");
		int sub = intInput();
		disksOnHex = disksOnHex + sub;
		disksOnTrack = disksOnTrack - sub;
		Output("How many disks did you remove from the hex?");
		int add = intInput();
		disksOnHex = disksOnHex - add;
		disksOnTrack = disksOnTrack + add;
		Output("----------------------------------");
	}

	public  void Research() {
			
	}

	public  void Upgrade() {
		
	}

	public  void Build() {
		boolean building = true;
		do {
			Output("You are able to build " + buildNum + " spacecraft. Please enter the number of each you are trying to build. (#intr#crsr#dred)");
			int input = intInput();
			char interceptors = Integer.toString(input).charAt(0);
			int interceptorsInt = Integer.parseInt(String.valueOf(interceptors));
			char cruisers = Integer.toString(input).charAt(0);
			int cruisersInt = Integer.parseInt(String.valueOf(cruisers));
			char dreadnought = Integer.toString(input).charAt(0);
			int dreadnoughtInt = Integer.parseInt(String.valueOf(dreadnought));
			int totalBuilds = interceptorsInt+cruisersInt+dreadnoughtInt;
			if (totalBuilds > buildNum && totalBuilds < 1) {
				Output("You cannot build that number of spacecraft.");
				building = true;
			}
			else {
				int TotalCost = interceptorsInt*Interceptor.getCost()+cruisersInt*Cruiser.getCost()+Dreadnought.getCost();
				if (TotalCost > TotalMaterials) {
					Output("You do not have enough materials to build this many spacecraft");
					building = true;
				}
				else {
					building = false;
					TotalMaterials = TotalMaterials-TotalCost;
					String added = "You have added ";
					if (interceptorsInt < 0)
						added = added + interceptorsInt + " interceptor(s), ";
					if (cruisersInt < 0)
						added = added + cruisersInt + " cruiser(s), ";
					if (dreadnoughtInt < 0)
						added = added + dreadnoughtInt + "dreadnought(s).";
					added = added + " You have " + TotalMaterials + " materials now.";
					Output(added);
				}
			}
		} while(building);
		Output("----------------------------------");
	}

	public  void Move() {
		Output("Your interceptors can move " + Interceptor.getMoveDistance());
		Output("Your cruisers can move " + Cruiser.getMoveDistance());
		Output("Your dreadnoughts can move " + Dreadnought.getMoveDistance());
		Output("----------------------------------");
	}

	public  void PlaceCubes() {
		Output("What cubes did you place? (ex. 110, 1 Money 1 Materials, 0 Science)");
		int input = intInput();
		char money = Integer.toString(input).charAt(0);
		int moneyInt = Integer.parseInt(String.valueOf(money));
		char materials = Integer.toString(input).charAt(0);
		int materialsInt = Integer.parseInt(String.valueOf(materials));
		char science = Integer.toString(input).charAt(0);
		int scienceInt = Integer.parseInt(String.valueOf(science));
		MoneyTrack.removeCubes(moneyInt);
		MaterialsTrack.removeCubes(materialsInt);
		ScienceTrack.removeCubes(scienceInt);
		Output("What cubes did you place back on the track?");
		money = Integer.toString(input).charAt(0);
		moneyInt = Integer.parseInt(String.valueOf(money));
		materials = Integer.toString(input).charAt(0);
		materialsInt = Integer.parseInt(String.valueOf(materials));
		science = Integer.toString(input).charAt(0);
		scienceInt = Integer.parseInt(String.valueOf(science));
		MoneyTrack.addCubes(moneyInt);
		MaterialsTrack.addCubes(materialsInt);
		ScienceTrack.addCubes(scienceInt);
		Output("----------------------------------");
	}

	public  void Pass() {
		
	}

	public  void CraftOutput() {
		Output("Your interceptors have these stats: \n" + Interceptor.toString());
		Output("----------------------------------");
		Output("Your cruisers have these stats: \n" + Cruiser.toString());
		Output("----------------------------------");
		Output("Your dreadnoughts have these stats: \n" + Dreadnought.toString());
		Output("----------------------------------");
	}
	
	public  void TechnoOutput() {
		
	}
	
	public  void diskTurnStart() {
		disksOnTrack = disksOnTrack + disksOnBoard;
		disksOnBoard = 0;
	}

	public  int costThisTurn() {
		int cost;
		if (disksOnTrack == 12)
			cost = 0;
		else if (disksOnTrack == 11)
			cost = 0;
		else if (disksOnTrack == 10)
			cost = 1;
		else if (disksOnTrack == 9)
			cost = 2;
		else if (disksOnTrack == 8)
			cost = 3;
		else if (disksOnTrack == 7)
			cost = 5;
		else if (disksOnTrack == 6)
			cost = 7;
		else if (disksOnTrack == 5)
			cost = 10;
		else if (disksOnTrack == 4)
			cost = 13;
		else if (disksOnTrack == 3)
			cost = 17;
		else if (disksOnTrack == 2)
			cost = 21;
		else if (disksOnTrack == 1)
			cost = 25;
		else if (disksOnTrack == 0)
			cost = 30;
		else
			cost = 100;
		return cost;
	}

	public static void Output(String out) {
		System.out.println(out);
	}

	public static int intInput() {
		Scanner scan = new Scanner(System.in);
		String entryString = scan.nextLine();
		int entry = Integer.parseInt(entryString);
		return entry;
	}

	public static String stringInput() {
		Scanner scan = new Scanner(System.in);
		String entry = scan.nextLine();
		return entry;
	}

	public  void InterceptorTest() {
		SpaceCraft Test = new SpaceCraft(1, 1, 0, 1, 0, 3, 1, 0, 0, 3, 0);
		System.out.println(Test.toString());
		Test.setMoveDistance(2);
		System.out.println("Move Distance now equals " + Test.getMoveDistance());
	}
}
