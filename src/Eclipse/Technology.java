package Eclipse;

public class Technology {
	String name, SpecialPower;
	int Shield, Computer, energySource, HullIncrease, YellowDice, OrangeDice, RedDice, MissileDice, discsAdded, 
		moveAdded, type, maxPrice, minPrice, buildIncrease, powerNeeded, speedIncrease, healEachRound;
	boolean status;
	
	public Technology (String title, int Shld, int Comp, int nrg, int Hull, int Yllw, int Orng,
			int Red, int Mssle, int discs, int move, String Spcl, int typ, int max, int min, int Bld,
			boolean Owned, int pwr, int spd, int heal) {
		name = title;
		Shield = Shld;
		Computer = Comp;int 
		energySource = nrg;
		HullIncrease = Hull;
		YellowDice = Yllw;
		OrangeDice = Orng;
		RedDice = Red;
		MissileDice = Mssle;
		discsAdded = discs;
		moveAdded = move;
		SpecialPower = Spcl;
		type = typ;
		maxPrice = max;
		minPrice = min;
		buildIncrease = Bld;
		status = Owned;
		powerNeeded = pwr;
		speedIncrease = spd;
		healEachRound = heal;
	}
	
	public Technology() {
		
	}
	
	public String getName() {
		return name;
	}
	
	public int getShield() {
		return Shield;
	}
	
	public int getComputer() {
		return Computer;
	}
	
	public int getEnergySource() {
		return energySource;
	}
	
	public int getHullIncrease() {
		return HullIncrease;
	}
	
	public int getYellowDice() {
		return YellowDice;
	}
	
	public int getOrangeDice() {
		return OrangeDice;
	}
	
	public int getRedDice() {
		return RedDice;
	}
	
	public int getMissileDice() {
		return MissileDice;
	}
	
	public int getDiscsAdded() {
		return discsAdded;
	}
	
	public int getMoveAdded() {
		return moveAdded;
	}
	
	public String getSpecialPower() {
		return SpecialPower;
	}
	
	public int getType() {
		return type;
	}
	
	public int getMaxPrice() {
		return maxPrice;
	}
	
	public int getMinPrice() {
		return minPrice;
	}
	
	public int getBuildIncrease() {
		return buildIncrease;
	}
	
	public boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean x) {
		status = x;
	}
	
	public int getPowerNeeded() {
		return powerNeeded;
	}
	
	public int getSpeedIncrease() {
		return speedIncrease;
	}
	
	public int getHealEachRound() {
		return healEachRound;
	}
}
