package Eclipse;

public class SpaceCraft {
	public int MoveDistance, NumOrangeDice, NumYellowDice, NumRedDice, UsablePower, TotalHP, TotalShield, TotalComp, Speed, Cost, NumMissilesDice;
	
	public SpaceCraft (int Move, int Spd, int Orange, int Yellow, int Red, int Pow, int HP, int Shld, int Comp, int MONEY, int Msls) {
		MoveDistance = Move;
		Speed = Spd;
		NumOrangeDice = Orange;
		NumRedDice = Red;
		NumYellowDice = Yellow;
		UsablePower = Pow;
		TotalHP = HP;
		TotalShield = Shld;
		TotalComp = Comp;
		Cost = MONEY;
		NumMissilesDice = Msls;
	}
	
	public void setMoveDistance (int x) {
		MoveDistance = x;
	}
	
	public void setNumOrangeDice (int x) {
		NumOrangeDice = x;
	}
	
	public void setNumYellowDice (int x) {
		NumYellowDice = x;
	}
	
	public void setNumRedDice (int x) {
		NumRedDice = x;
	}
	
	public void setUsablePower (int x) {
		UsablePower = x;
	}
	
	public void setTotalHP (int x) {
		TotalHP = x;
	}
	
	public void setTotalShield (int x) {
		TotalShield = x;
	}
	
	public void setTotalComp (int x) {
		TotalComp = x;
	}
	
	public void setSpeed (int x) {
		Speed = x;
	}
	
	public void setNumMissilesDice (int x) {
		NumMissilesDice = x;
	}
	
	public int getMoveDistance () {
		return MoveDistance;
	}
	
	public int getNumOrangeDice () {
		return NumOrangeDice;
	}
	
	public int getNumYellowDice () {
		return NumYellowDice;
	}
	
	public int getNumRedDice () {
		return NumRedDice;
	}
	
	public int getUsablePower () {
		return UsablePower;
	}
	
	public int getTotalHP () {
		return TotalHP;
	}
	
	public int getTotalShield () {
		return TotalShield;
	}
	
	public int getTotalComp () {
		return TotalComp;
	}
	
	public int getSpeed () {
		return Speed;
	}
	
	public int getCost () {
		return Cost;
	}
	
	public int getNumMissilesDice () {
		return NumMissilesDice;
	}
	
	public String toString() {
		String info = null;
		info = "Move Distance: " + MoveDistance + "\nSpeed: " + Speed + "\nTotal HP: " + TotalHP + "\nNumber of Orange Dice: "
				+ NumOrangeDice + "\nNumber of Yellow Dice: " + NumYellowDice + "\nNumber of Red Dice: " + NumRedDice +
				"\nUsable Power: " + UsablePower + "\nTotal Shield: -" + TotalShield + "\nTotal Computer: +" + TotalComp;
		return info;
	}
}
